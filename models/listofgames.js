const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// Game Schema
const SchemaOfGame = mongoose.Schema({
  gid: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  platform: {
    type: String,
    required: true
  },  
  gamepic:{
    type: String,
    required: true
  },
  desc:{
    type: String
  },
  shortDesc:{
    type: String,
    required: true
  },
  characters:{
    type: String,
    required: true
  }
});

const Game = module.exports = mongoose.model('gamelists', SchemaOfGame);

module.exports.getAllGamelist = function(callback){ 
   Game.find(callback);
}

module.exports.getGameDetail = function(id,callback){ 
   const query = {_id: id}
   Game.findOne(query, callback);
}

module.exports.saveGamelist = function(newGame, callback){ 
   newGame.save(callback);
}

module.exports.updateGamelist = function(newGame, callback){ 
   const query = {name: newGame.name,platform: newGame.platform,gamepic: newGame.gamepic,desc: newGame.desc,shortDesc: newGame.shortDesc,characters:newGame.characters};
   newGame.update({name: newGame.name},{ $set: query}, callback);
}