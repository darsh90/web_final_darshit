const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const UserSchema = mongoose.Schema({
    r_id: {
        type: String
    },
    username: {
        type: String
    },
    useremail: {
        type: String
    },
    username: {
        type: String
    },
    userpassword: {
        type: String
    },
    // birthday: {
    //     type:String
    // },
    usergender: {
        type: String
    },
    emailVerifyofuser: {
        type: Boolean
    },
    userprofilepic: {
        type: String
    },
    userprovider: {
        type: String
    },
    fg_id: {
        type: String
    },
    userhobbies: {
        type:String
    },
    userregularTime:{
        type:String
    },
    usermic:{
        type:Boolean
    },
    usergameInterest:{
        type:String
    },
    userplatform:{
        type:String
    },
    userpersonal_desc:{
        type:String
    },
    fg_image:{
        type:String
    },
    userfriendsID:{
        type:String
    },
    userrecDate: {
        type: String
    },
    isUserActive:{
        type:Boolean
    }
});


const GameUser = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function(id, callback) {
    GameUser.findById(id, callback);
};

module.exports.getUserByUsername = function(username, callback) {
    const query = {
        username: username
    };
    GameUser.findOne(query, callback);
};

module.exports.getUseremailVerify = function(emailverify, callback) {   
    GameUser.find(callback).where('emailVerify', emailverify).select('emailverify');
};

module.exports.checkemailVerify = function(email, callback) {   
    GameUser.find(callback).where('email', email).select('emailverify');
};

module.exports.getUserFoundByEmail = function(email, callback) {
    GameUser.find(callback).where('email', email).select('email');
};

module.exports.getUserFoundByEmailAid = function(newUser, callback) {
    GameUser.find(callback).where('email', newUser.email).select('email');
};

module.exports.getUserGameInterest = function(newUser, callback) {    
    GameUser.find(callback).where('gameInterest', newUser.gameInterest);
};

module.exports.getUserFoundByFGID = function(fg_id, callback) {    
    GameUser.find(callback).where('fg_id', fg_id).select('fg_id username');
};

module.exports.getMatchFg_id = function(newUser, callback) {
    const query = {
        fg_id: { $gt: newUser.fg_id }
    };
    GameUser.bios.find(query, callback);
};

module.exports.getMatchVerify = function(newUser, callback) {
    GameUser.find(callback).where('email', newUser.email).where('emailVerify', newUser.emailVerify).select('emailVerify');
};

module.exports.getUserCount = function(callback) {
    GameUser.count(callback);
};

module.exports.updateUser = function(newUser, callback) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            GameUser.update({
                email: newUser.email
            }, {
                $set: {
                    password: newUser.password
                }
            }, callback);
        });
    });
};

module.exports.updateVerification = function(newUser, callback){
    GameUser.update({
        email: newUser.email
    }, {
        $set: {
            emailVerify: newUser.emailVerify
        }
    }, callback);
};

// module.exports.getUserfrdDt = function(id,callback){
//     User.find(callback).where('_id', newUser._id);
// };

module.exports.addUser = function(newUser, callback) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
};

module.exports.comparePassword = function(candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
};

module.exports.compareFGID = function(uid, callback){
    GameUser.find(callback).where('fg_id', uid);
};

module.exports.EUserFG = function(newUser, callback) {
    GameUser.update({
        email: newUser.email
    }, {
        $set: {
            fg_id: newUser.fg_id,
            profilepic: newUser.profilepic,
            provider: newUser.provider
        }
    }, callback);
};
module.exports.NEUserFG = function(newUser, callback) {
    newUser.save(callback);
};