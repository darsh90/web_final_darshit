const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');


// postGame Schema
const SchemaForPostGames = mongoose.Schema ({
  gp_id:{
    type: String 
  },
  nameofgame:{
    type:String
  },
  gamename:{
    type: String
  },
  userregion: {
    type: String 
  },
  gameplatform: {
    type: String
  },  
  userlanguage:{
    type: String
  },
  useractivity:{ 
    type: String 
  },
  usernote:{
    type: String
  },
  userlookingfor:{
    type: String
  },
  usermic:{
    type: Boolean
  },
  gamecharacters:{
    type: String
  },
  userrecDate: {
    type: String
  },
  isUserActive:{
    type:Boolean
  } 
});


const userPostGame = module.exports = mongoose.model('postgame', SchemaForPostGames);

module.exports.getAllPostGlist = function(callback){ 
   userPostGame.find(callback).where('isActive', false).sort({ recDate: 1, _id: 'desc' });
}
module.exports.getActivityPostGlist = function(newPostGame, callback){ 
   userPostGame.find(callback).where('isActive', false).where('name', newPostGame.name).sort({ recDate: 1, _id: 'desc' });
}

module.exports.repostrquery = function(newPostGame, callback){  
    userPostGame.update({_id: newPostGame._id}, { $set:{ recDate: newPostGame.recDate }},callback);
}

module.exports.Delpostrquery = function(newPostGame, callback){  
    userPostGame.update({_id: newPostGame._id}, { $set:{ recDate: newPostGame.recDate, isActive: true }},callback);     
}

module.exports.getFilterPostGlist = function(newPostGame, callback){ 
    const query = {$or:[{platform: newPostGame.platform},{gamename: newPostGame.gamename},{characters: newPostGame.characters}]}
     userPostGame.find(query, callback).sort({ recDate: 1,  _id: 'desc' });
}

module.exports.getPostOne = function(platform, callback){ 
   const query = {platform: platform}
   userPostGame.findOne(query, callback);
}

module.exports.savePostGame = function(newPostGame, callback){ 
   newPostGame.save(callback);
}