import { GameappsPage } from './app.po';

describe('gameapps App', () => {
  let page: GameappsPage;

  beforeEach(() => {
    page = new GameappsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
