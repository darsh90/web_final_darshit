import { Injectable } from '@angular/core';

@Injectable()
export class UserValidateService {

  constructor() { }
  uservalidateRegister(user){
    if(user.name == undefined || user.email == undefined || user.username == undefined || user.password == undefined){
      return false;
    } else {
      return true;
    }
  }

  uservalidateReset(user){
    if(user.email == undefined || user.password == undefined || user.cpassword == undefined){
      return false;
    } else {
      return true;
    }
  }

  uservalidateLogin(user){
    if(user.username == undefined || user.password == undefined){
      return false;
    } else {
      return true;
    }
  }

  uservalidateEmail(email){
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  uservalidatePassword(password){   
    const re = /[a-z]\d|\d[a-z]/i;
    return re.test(password) && password.length > 8;
  }

}
