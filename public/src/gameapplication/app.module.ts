import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { PageOfHomeComponent } from './components/pageofhome/pageofhome.component';
import { PageOfAboutComponent } from './components/pageofabout/pageofabout.component';
import { PageofLoginComponent } from './components/pageoflogin/pageoflogin.component';
import { PageOfProfileComponent } from './components/pageofprofile/pageofprofile.component';
import { PageOfGamesComponent } from './components/pageofgames/pageofgames.component';
import { UserResetComponent } from './components/pageofreset/pageofreset.component';
import { userRegisterComponent } from './components/pageofregister/pageofregister.component';
import { PageOfSliderComponent } from './components/pageofhome/pageofhome.component';
import { PageOfGamesdetailComponent } from './components/pageofgamesdetail/pageofgamesdetail.component';
import { PageofLfgComponent } from './components/pageoflfg/pageoflfg.component';
import { PageOfEmailverifyComponent } from './components/pageofemailverify/pageofemailverify.component';

import { UserPostService } from './userpost.service';
import { AuthGuard } from './auth.guard';
import { UserValidateService } from './uservalidate.service';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { Angular2SocialLoginModule } from "angular2-social-login";
import { MomentModule } from 'angular2-moment';
import { MyDatePickerModule } from 'mydatepicker';


//CLient ID of Google+ (221475810444-ad5gmvg8q5v1ekrgd1a44bgoim4kn3rq.apps.googleusercontent.com)
//App ID of Facebook (1801743403476436) 

const providers = {
    "google": {
      "clientId": "221475810444-ad5gmvg8q5v1ekrgd1a44bgoim4kn3rq.apps.googleusercontent.com"
    },
    "facebook": {
      "clientId": "1801743403476436",
      "apiVersion": "v2.8"
    }
};

const appRoutes: Routes = [
  { path: '', component: PageOfHomeComponent },
  { path: 'register', component: userRegisterComponent },
  { path: 'login', component: PageofLoginComponent },
  { path: 'reset', component: UserResetComponent, },
  { path: 'about', component: PageOfAboutComponent },
  { path: 'slider', component: PageOfSliderComponent },
  { path: 'games', component: PageOfGamesComponent },
  { path: 'games/:id', component: PageOfGamesdetailComponent, canActivate: [AuthGuard] },
  { path: 'emailverify/:id', component: PageOfEmailverifyComponent},
  { path: 'profile', component: PageOfProfileComponent, canActivate: [AuthGuard] },
  { path: 'lfg', component: PageofLfgComponent, canActivate: [AuthGuard] }
]


@NgModule({
  declarations: [
    AppComponent,
    PageOfHomeComponent,
    PageOfAboutComponent,
    PageofLoginComponent,
    PageOfProfileComponent,
    PageOfGamesComponent,
    UserResetComponent,
    userRegisterComponent,
    PageOfSliderComponent,
    PageOfGamesdetailComponent,
    PageofLfgComponent,
    PageOfEmailverifyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    Angular2SocialLoginModule.initWithProviders(providers),  
    FlashMessagesModule,
    MomentModule,
    MyDatePickerModule 
  ],
  providers: [UserPostService,AuthGuard,UserValidateService],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
