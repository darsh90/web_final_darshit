import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { PostService } from '../../post.service';
import { ValidateService } from '../../validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})

export class ResetComponent implements OnInit {
  email: String;
  password: String;
  cpassword: String;

  constructor(
    private router:Router,
    private postService: PostService, 
    private validateService: ValidateService,
    private flashMessage:FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onResetSubmit(){   
    const user = {
      email: this.email
    }
    
    // Validate Email
    if(!this.validateService.validateEmail(user.email)){
      this.flashMessage.show('Please use a valid email', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }    
    
    this.postService.resetPassword(user).subscribe(data => {
      if(data.success){        
        this.flashMessage.show('Successfully Reset Password', {cssClass: 'alert-success',timeout: 3000});
        this.flashMessage.show('Please Check Mail for New Password Generated', {cssClass: 'alert-success',timeout: 3000});        
      } else {
        this.flashMessage.show('Failed to Reset the Password', {cssClass: 'alert-danger',timeout: 5000});
      }
    });
  }   

  
}
