import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageofLoginComponent } from './pageoflogin.component';

describe('LoginComponent', () => {
  let component: PageofLoginComponent;
  let fixture: ComponentFixture<PageofLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageofLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageofLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
