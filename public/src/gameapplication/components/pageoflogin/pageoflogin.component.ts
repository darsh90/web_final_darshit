import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { PostService } from '../../post.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ValidateService } from '../../validate.service';
import { AuthService } from "angular2-social-login";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class PageofLoginComponent implements OnInit {
  name: String;
  username: String;
  email: String;
  password: String;
  profilepic:String;
  gender:String;
  sub: any;

  public userData: any;
  public userToken: any;
  public userJson: any;
  public accountname: String;
   
  public user: String;

  constructor(
    private validateService: ValidateService,
    private router:Router,
    private flashMessage:FlashMessagesService,
    private postService:PostService,
    public _auth: AuthService
    ) { }

  ngOnInit() {
      //this.userData = JSON.parse(localStorage.getItem("user"));
      //console.log(this.userData.username);
      //if(this.userData.username != ""){
         //this.user=this.userData.username; 
     // }
  }

  onLoginSubmit(){   
    const user = {
      username: this.username,
      password: this.password
    }
    // Required Fields
    if(!this.validateService.validateLogin(user)){
      this.flashMessage.show('Please fill in all fields', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    // Validate password
    if(!this.validateService.validatePassword(user.password)){
      this.flashMessage.show('Please use a valid password. Password should be more than 8 character with number and special character', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    this.postService.authenticateUser(user).subscribe(data => {
      if(data.success){
        this.postService.storeUserData(data.token, data.user);
        this.flashMessage.show('You are now logged in', { cssClass: 'alert-success',timeout: 5000});
        this.router.navigate(['profile']);
      } else {
        this.flashMessage.show(data.msg, {cssClass: 'alert-danger',timeout: 5000});
        this.router.navigate(['login']);
      }
    });
  }

  
  signIn(provider){
    console.log(provider);
    this._auth.login(provider).subscribe(data =>{ 
        console.log(data);
        this.postService.loginWithFG(data).subscribe(data => {
            console.log(data);
            if(data.success){
              this.postService.storeUserData(data.token, data.user);
              this.flashMessage.show('You are now registered by '+ provider, {cssClass: 'alert-success', timeout: 3000});              
              this.router.navigate(['profile']);
            } else {
              this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
              this.router.navigate(['/login']);
            }
          });
    });    
  }  

  

}
