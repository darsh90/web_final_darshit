import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesdetailComponent } from './gamesdetail.component';

describe('GamesdetailComponent', () => {
  let component: GamesdetailComponent;
  let fixture: ComponentFixture<GamesdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
