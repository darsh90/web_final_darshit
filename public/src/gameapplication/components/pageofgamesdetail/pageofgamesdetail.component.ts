import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../../post.service';


@Component({
  selector: 'app-gamesdetail',
  templateUrl: './gamesdetail.component.html',
  styleUrls: ['./gamesdetail.component.css']
})

export class PageOfGamesdetailComponent implements OnInit {
  id: number;
  public _id: String;
  public username :String;
  public gameplatform :String;
  public gamepic :String;
  public gameDesc :String;
  public gameshortDesc :String;
  public gamecharacters :String;
  
  public userID:String;
  public userdataobj:Object;
  public matchfound:Boolean;
  public temp:Boolean;

  constructor(
    private postService: PostService,
    private router: Router,
    public route: ActivatedRoute
  ) { }

  ngOnInit() {       
    this.route.params.subscribe(params => {
      if (params['id']) {
        console.log(params['id']);
        this.postService.getGameDetail(params['id']).subscribe(data => { 
          
          this._id = data.game._id;
          this.username = data.game.name;
          this.gameplatform = data.game.platform;
          this.gamepic = data.game.gamepic;
          this.gameDesc = data.game.desc;
          this.gameshortDesc= data.game.shortDesc;
          this.gamecharacters= data.game.characters;

          this.getUserdetails(this.username);
          this.UserAddInterestShow(this.username);
          
          //console.log(this.name); 
        }); 
      }
    });
  }

  getUserdetails(name){
     const user = {
      gamenm: name
    }         
    this.postService.getGameInterest(user).subscribe(data => {            
        this.userID = data.user;
        //console.log(this.userID);            
    });
  }
  
  UserAddInterestShow(name){    
     const user = {
      gamenm: name
    }         
    this.postService.getGameInterest(user).subscribe(data => { 
        for (var v in data.user) { 
            console.log( data.user[v].gameInterest);
            this.userdataobj = data.user[v].gameInterest;
            for (var t in data.user[v].gameInterest) {
                if(data.user[v].gameInterest[t] === user.gamenm) {
                    this.matchfound = true;
                    if(this.matchfound){
                       this.temp= true;
                    }
                    //console.log(t + "Match");
                }else{
                    this.matchfound = false;
                    //console.log(t +  "not match");
                }
            }            
        }          
    });
  }


  userinterestfun(temp){
      console.log(temp);
       const user = {
          gamenm: true
        } 
      this.postService.Interestfunc(user).subscribe(data => {            
          this.userID = data.user;
          console.log(this.userID);            
      });
      
  } 

}
