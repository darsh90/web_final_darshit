import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../../post.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-emailverify',
  templateUrl: './emailverify.component.html',
  styleUrls: ['./emailverify.component.css']
})
export class PageOfEmailverifyComponent implements OnInit {
  public gameuser: Object;
  public userverifydata: Object;
  public gameuserData: String;
  public userverify: String;
  public useremail: String;

  constructor(
    private gamepostService: PostService,
    private gamerouter: Router,
    private userflashMessage: FlashMessagesService,
    public userroute: ActivatedRoute
  ) { }

  ngOnInit() {
    

    this.userroute.params.subscribe(params => {
      if (params['id']) {
        
        this.gameuserData = JSON.parse(localStorage.getItem("user"));
           
         const user = {
            email : this.gameuserData['email'],
            emailVerify : params['id']
         }

        this.gamepostService.verifyUpdated(user).subscribe(data => {
            if (data.success) {
                this.userflashMessage.show('Verification Successfully!', { cssClass: 'alert-success', timeout: 5000 });
            } else {
                this.userflashMessage.show('Failed to Verify Email!', { cssClass: 'alert-danger', timeout: 5000 });
            }
        });
      }
    });
  }
  
}
