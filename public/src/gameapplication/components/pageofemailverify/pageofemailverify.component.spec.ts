import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageOfEmailverifyComponent } from './pageofemailverify.component';

describe('EmailverifyComponent', () => {
  let component: PageOfEmailverifyComponent;
  let fixture: ComponentFixture<PageOfEmailverifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageOfEmailverifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageOfEmailverifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
