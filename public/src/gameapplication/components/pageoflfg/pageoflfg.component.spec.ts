import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageofLfgComponent } from './pageoflfg.component';

describe('LfgComponent', () => {
  let component: PageofLfgComponent;
  let fixture: ComponentFixture<PageofLfgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageofLfgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageofLfgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
