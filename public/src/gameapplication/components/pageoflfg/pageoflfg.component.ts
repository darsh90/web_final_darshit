import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from '../../post.service';
import { MomentModule } from 'angular2-moment';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-lfg',
  templateUrl: './lfg.component.html',
  styleUrls: ['./lfg.component.css']
})


export class PageofLfgComponent implements OnInit {
  userpostgame: String;
  countofonlineMember: String;
  game: String;
  gameusername:String;
  gamename: String;
  userpersonname: string;
  gameplatform: String;
  userregion: String;
  userlanguage: String;
  useractivity: String;
  userlookingfor: String;
  usermic: String;
  gamecharacters: String;
  gamenote: String;
  userData: String;
  useractivitys:String;

  constructor(
    private userpostService: PostService,
    private userrouter: Router,
    private userflashMessage: FlashMessagesService
  ) { 
      
    

  }

  ngOnInit() {    
    this.userfilterGamePostsubmit("ALL");
    this.UserActivityrGamePostlist();
    this.userpostService.getMemberCount().subscribe(data => {
      this.countofonlineMember = data.user;
    });
    this.userpostService.getGameList().subscribe(data => {
      this.game = data.game;
    });
  }

    

  onUserGamePostSubmit() {
    this.userData = JSON.parse(localStorage.getItem("user"));
    this.gameusername= this.userData['username'];

    const user = {
      name: this.userData['username'],
      platform: this.gameplatform,
      region: this.userregion,
      language: this.userlanguage,
      activity: this.useractivity,
      lookingfor: this.userlookingfor,
      note: this.gamenote,
      mic: this.usermic,
      characters: this.gamecharacters,
      gamename: this.gamename
    }

    this.userpostService.postGames(user).subscribe(data => {
      if (data.success) {
        this.userflashMessage.show('Successfully Post', { cssClass: 'alert-success', timeout: 3000 });       
        this.userfilterGamePostsubmit("ALL");
        this.UserActivityrGamePostlist();
      } else {
        this.userflashMessage.show('Something went wrong', { cssClass: 'alert-danger', timeout: 3000 });
      }
    });
  }

  userfilterGamePostsubmit(data){
      if(data==="ALL"){
          this.userpostService.getPostGameList().subscribe(data => {
            this.userpostgame = data.postgame;
          });
      }else{
          const user = {
            platform: this.gameplatform,        
            gamename: this.gamename,
            characters:this.gamecharacters
          }
          this.userpostService.filterPostGame(user).subscribe(data => {
              this.userpostgame = data.postgame; 
          });
      }
      
  }

  UserActivityrGamePostlist(){ 
      this.userData = JSON.parse(localStorage.getItem("user"));
      this.gameusername= this.userData['username'];

      const user = {
        username: this.gameusername
      }
      this.userpostService.ActivtyPostGame(user).subscribe(data => {
          this.useractivitys = data.postgame;
          console.log(this.useractivitys); 
      });
  }



  userresetGamepost(){
    this.userfilterGamePostsubmit("ALL");
    this.UserActivityrGamePostlist();
  }
  
  userrepostGame(id){    
    const user = {
      _id: id   
    }
    console.log(user);
    this.userpostService.RepostGames(user).subscribe(data => {
       console.log(data);
       this.userfilterGamePostsubmit("ALL");
       this.UserActivityrGamePostlist();
    });  
  }

  userDeletepostGame(id){   
    const user = {
      _id: id    
    }
    console.log(user);
    this.userpostService.DelpostGames(user).subscribe(data => {
       console.log(data);
       this.userfilterGamePostsubmit("ALL");
       this.UserActivityrGamePostlist();
    });  
  }
}
