import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../../post.service';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})

export class PageOfProfileComponent implements OnInit {
    gameuser: Object;
    public verifydata: Object;
    gameuserData: String;
    userverify: String;
    useremail: String;
    usergame: String;
    userfrd: String;
    userfrds: String;

    
    constructor(
        private postService: PostService,
        private router: Router,
        private flashMessage: FlashMessagesService,
        public route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.gameuserData = JSON.parse(localStorage.getItem("user"));
        //console.log(this.userData['email']);     

        this.postService.getProfile().subscribe(data => {
            this.gameuser = data.user;
            //   this.frd = data.user['friendsID'];
            //   this.postService.getfrdDt(this.frd).subscribe(data => {          
            //      this.frds = data.user;  
            //      console.log(this.frds);           
            //   });
        });
        
        this.postService.getGameList().subscribe(data => {
            this.usergame = data.game;
        });

        const verifydata = {
            email: this.gameuserData['email'],
            emailVerify: true
        }
        //verify email 
        this.postService.postverifyProfile(verifydata).subscribe(data => {
            if (data) {
                if (data.success == false) {
                    this.userverify = data.success;
                } else if (data.success == true) {
                    this.userverify = data.success;
                }
            }
        });
    }

   
    
}
