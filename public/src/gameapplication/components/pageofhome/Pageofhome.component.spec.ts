import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageOfHomeComponent } from './pageofhome.component';

describe('HomeComponent', () => {
  let component: PageOfHomeComponent;
  let fixture: ComponentFixture<PageOfHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageOfHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageOfHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
