import { Component, OnInit } from '@angular/core';
import { PostService } from '../../post.service';
@Component({
  selector: 'app-home',
  templateUrl: './pageofhome.component.html',
  styleUrls: ['./pageofhome.component.css']
})
export class PageOfHomeComponent implements OnInit {
  constructor(private postService:PostService) { }
  ngOnInit() {
    
  }
 
    usermailsend(){
      const user = {
        email: "darshitpatel.90@gmail.com"
      }
      this.postService.mailSend(user).subscribe(data => {
        console.log(data);
      });
    }
}

@Component({
  selector: 'app-Slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./home.component.css']
})
export class PageOfSliderComponent {
  constructor() {  }
  public gameimages: { url: string }[] = [
    { "url": "../../assets/img/1.jpg" },
    { "url": "../../assets/img/2.jpg" },
    { "url": "../../assets/img/3.jpg" },
    { "url": "../../assets/img/4.jpg" },	
    { "url": "../../assets/img/5.jpg" },
    { "url": "../../assets/img/6.jpg" }
  ];
}