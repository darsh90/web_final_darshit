import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { PostService } from '../../post.service';


@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})


export class PageOfGamesComponent implements OnInit {
   usergame:String;
  
  constructor(
    private userpostService: PostService,
    private userrouter:Router
    ) { }

  ngOnInit() {
      this.userpostService.getGameList().subscribe(data => {
          console.log(data);
          this.usergame = data.game;
      });
    
    }
}
