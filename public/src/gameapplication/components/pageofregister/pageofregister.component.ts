import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from '../../post.service';
import { ValidateService } from '../../validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { IMyOptions } from 'mydatepicker';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit {
  name: String;
  username: String;
  email: String;
  password: String;
  profilepic: String;
  gender: String;
  birthday:String;

  constructor(
    private validateService: ValidateService,
    private postService: PostService,
    private router: Router,
    private flashMessage: FlashMessagesService,
  ) { }

  ngOnInit() {
  }

   private myDatePickerOptions: IMyOptions = {
        dateFormat: 'dd/mm/yyyy', 
    };


  onRegisterSubmit() {
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password,
      gender: this.gender,
      profilepic: "default.png"
      //birthday: this.birthday
    }

    // Required Fields
    if (!this.validateService.validateRegister(user)) {
      this.flashMessage.show('Please fill in all fields', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }

    // Validate Email
    if (!this.validateService.validateEmail(user.email)) {
      this.flashMessage.show('Please use a valid email', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }

    // Validate password
    if (!this.validateService.validatePassword(user.password)) {
      this.flashMessage.show('Please use a valid password. Password should be more than 8 character with number and special character', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }

    // Register user
    this.postService.registerUser(user).subscribe(data => {
      if (data.success) {
        this.flashMessage.show('Please Verfiy Email Address for Registration Account!', { cssClass: 'alert-success', timeout: 3000 });
        // this.postService.mailSend(user).subscribe(data => {
        //   if (data.success) {
        //     console.log(data);
        //   }
        // });
      } else {
        console.log(data);
        if(data.msg == false){
            this.flashMessage.show('Email is already Exist', { cssClass: 'alert-danger', timeout: 3000 });            
        }else{
            this.flashMessage.show('Something went wrong', { cssClass: 'alert-danger', timeout: 3000 });            
        }
        this.router.navigate(['/register']);
      }
    });

  }


}
