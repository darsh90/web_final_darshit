import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { userRegisterComponent } from './pageofregister.component';

describe('RegisterComponent', () => {
  let component: userRegisterComponent;
  let fixture: ComponentFixture<userRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ userRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(userRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
