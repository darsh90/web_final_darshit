import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import { PostService } from './post.service';

@Injectable()
export class AuthGuard implements CanActivate{
  constructor(private postService: PostService, private router:Router){

  }

  canActivate(){
    if(this.postService.loggedIn()){
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
