import { Component, OnInit } from '@angular/core';
import { PostService } from './post.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  

  constructor(
    private postService: PostService,
    private router: Router,
    private flashMessage: FlashMessagesService
  ) {
      
      
  }
  ngOnInit() {
      
  }
  onLogoutClick() {
    this.postService.logout();
    this.router.navigate(['/login']);
    return false;
  }
}
