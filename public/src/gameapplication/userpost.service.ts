 'Use Strick';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {tokenNotExpired} from 'angular2-jwt';
//import { HttpClient } from './http-client';
//import { HttpClient } from '../../shared/http/base.http';

@Injectable()
export class PostService {
  authToken: any;
  user: any;
  private projectUrl = '/users/';

  constructor(private http: Http) { 
    
  }  

  registerUser(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/register', user, {headers: headers})
      .map(res => res.json());
  }

  loginWithFG(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/fg', user, {headers: headers})
      .map(res => res.json()); 
  }

  mailSend(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/mail', user, {headers: headers})
      .map(res => res.json());
  }

  authenticateUser(user){   
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/authenticate', user,{headers: headers})
      .map(res => res.json());
  }

  filterPostGame(user){
      let headers = new Headers();
      headers.append('Content-Type','application/json');
      return this.http.post('http://localhost:3000/users/lfgs', user, {headers: headers})
        .map(res => res.json());
  }
  ActivtyPostGame(user){
      let headers = new Headers();
      headers.append('Content-Type','application/json');
      return this.http.post('http://localhost:3000/users/activity', user, {headers: headers})
        .map(res => res.json());
  }
  
  postGames(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/lfg', user, {headers: headers})
      .map(res => res.json());
  }

  getGameInterest(user){
    let headers= new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/gi', user, {headers: headers})
      .map(res => res.json());       
  }

  Interestfunc(user){
      let headers = new Headers();
      headers.append('Content-Type','application/json');
      return this.http.post('http://localhost:3000/users/gi', user, {headers: headers})
        .map(res => res.json());
  }

  RepostGames(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/repost', user, {headers: headers})
      .map(res => res.json());
  }

  DelpostGames(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/delpost', user, {headers: headers})
      .map(res => res.json());
  }
  
  getPostGameList(){
    let headers= new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get('http://localhost:3000/users/lfg', {headers: headers})
      .map(res => res.json());       
  }

  getProfile(){  
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this.http.get('http://localhost:3000/users/profile', {headers: headers})
      .map(res => res.json()); 
  }
  // getfrdDt(frd){
  //    let headers = new Headers();
  //    headers.append('Content-Type','application/json');
  //    return this.http.post('http://localhost:3000/users/frdDts', frd, {headers: headers})
  //     .map(res => res.json()); 
  // }
  
  postverifyProfile(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/profiles', user, {headers: headers})
      .map(res => res.json()); 
  }
  verifyUpdated(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/verifyemail', user, {headers: headers})
      .map(res => res.json());
  }
  getMemberCount(){
    let headers= new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get('http://localhost:3000/users/home', {headers: headers})
      .map(res => res.json()); 
  }

  getGameList(){
    let headers= new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get('http://localhost:3000/users/games', {headers: headers})
      .map(res => res.json()); 
      // return this.http.get(this.projectUrl+'games')
      //            .toPromise()
      //            .then(response => response.json())
      //            .catch(this.handleError);
  }

  getGameDetail(id){
    let headers= new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get('http://localhost:3000/users/games/'+id, {headers: headers})
      .map(res => res.json());      
  }

  storeUserData(token, user){
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn(){
    return tokenNotExpired();
  }

  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  resetPassword(user){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/reset', user, {headers: headers})
      .map(res => res.json());
  }  
  

  private handleError (error: any) {
      let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
      console.error(errMsg); // log to console instead
    }
  
}
