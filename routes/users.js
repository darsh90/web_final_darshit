'Use Strick';
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');
const Game = require('../models/gamelist');
const postGame = require('../models/postgame');
const shortid = require('shortid');
const nodemailer = require('nodemailer');
const xoauth2 = require('xoauth2');
var generator = require('generate-password');



// const transporter = nodemailer.createTransport({
//     service: "Gmail",
//     auth: {
//         xoauth2: xoauth2.createXOAuth2Generator('SMTP',{
//             user: 'denishp83@gmail.com',
//             pass: 'wampserver',
//             clientId: '221475810444-q5vqhtsvnq5u04d6aa1krbtain2l6skr.apps.googleusercontent.com',
//             clientSecret: 'HYd98AgAV8jMcxHsd0Xxz6Sv',
//             refreshToken: '1/_6Rh5LaQoMm8T8DNRMcijlF5Fb99SxQbRzC1bnvDxmHf9UzxAa0BeuzrgZZkCLdf'
//         })
//     }
// });
// create reusable transporter object using the default SMTP transport

let usertransporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'denishp83test@gmail.com',
        pass: 'wampserver'
    }
});


function usermail(emailinfo){
    const mailOptions = {
        from: 'denishp83test@gmail.com',
        to: emailinfo.email,
        subject: emailinfo.subject,
        generateTextFromHTML: true,
        html: emailinfo.body
    };
    // send mail with defined transport object
    usertransporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            return res.json({
                success: false
            });
        } else {
            return res.json({
                success: true
            });
        }
        //console.log('Message %s sent: %s', info.messageId, info.response);
    });
}

// router.post('/mail', (req, res, next) => {
//     const mailOptions = {
//         from: 'denishp83test@gmail.com',
//         to: 'denishp83@gmail.com',
//         subject: 'Hello',
//         generateTextFromHTML: true,
//         html: '<b>Hello world</b>'
//     };
//     // send mail with defined transport object
//     transporter.sendMail(mailOptions, (error, info) => {
//         if (error) {
//             console.log(error);
//         } else {
//             console.log(response);
//         }
//         //console.log('Message %s sent: %s', info.messageId, info.response);
//     });    
// });


// Register
userrouter.post('/register', (req, res, next) => {
    //console.log(shortid.generate());
    let newUser = new User({
        r_id: shortid.generate(),
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        gender: req.body.gender,
        profilepic: req.body.profilepic,
        //birthday: req.body.birthday,
        emailVerify: false,
        provider: null,
        fg_id: null,
        hobbies: null,
        regularTime: null,
        mic: null,
        gameInterest: null,
        platform: null,
        personal_desc: null,
        fg_image: null,
        friendsID: null,
        recDate: new Date(),
        isActive: false
    });
    
    User.getUserFoundByEmail(req.body.email,(err, user) => {
       // if (err) throw err;
        if(user != ""){
            return res.json({
                success: false,
                msg: false
            });
        }else if(user == ""){
            User.addUser(newUser, (err, user) => {
                if (err) {
                    return res.json({
                        success: false
                    });
                } else {
                    const emailinfo = {
                        email:req.body.email,
                        subject:'Game apps : Register with Game apps',
                        body:`
                            <p>Hi `+ req.body.email +`,</p>                                
                                <p>Welcome to Game apps!</p>
                                <p>Thanks for choosing Game apps. We're happy to assist you on your Games journey!</p>
                                <p>Your free Game app account has been created and is ready to use.</p>
                                <label>Your Register Email Address: </label>
                                <h3 styles="margin:0px;">`+ req.body.email +`</h3>
                                <label>Your Username: </label>
                                <h3 styles="margin:0px;">`+ req.body.name +`</h3>
                                
                                <p>Email Verification Link</p>
                                <a href="http://localhost:4200/emailverify/true">Click Here</a>
                                
                                <p>All the best, The team at Annabel.in</p>
                                <p>For any further queries/assistance, kindly mail us at denishp83test@gmail.com</p>                                
                                <p>Enjoy!</p>
                                <p>The Team Annabel.in</p>
                         `
                    };
                    usermail(emailinfo);
                    return res.json({
                        success: true
                    });
                }
            });    
         }
    });
});

//facebook google login
userrouter.post('/fg', (req, res, next) => { 
    User.getUserFoundByFGID(req.body.uid, (err, user) => {
        if (err) throw err;
        if(user != ""){            
            //login with f and g
            User.compareFGID(req.body.uid, (err, isMatch) => {
                if (err) throw err;
                if (isMatch) {
                    console.log(isMatch);
                    res.json({
                    success: true,
                    token: 'JWT ' + req.body.token,
                    user: {
                        id: user._id,
                        name: user.name,
                        //username: user.username,
                        email: user.email
                    }
                });
                } else {
                    return res.json({
                        success: false,
                        msg: 'Wrong password'
                    });
                }
            });
        }else if(user == ""){
            User.getUserFoundByEmail(req.body.email, (err, user) => {
                console.log(user);
                if (err) throw err;
                if(user != ""){
                    // email found
                   let newUser = new User({
                        fg_id: req.body.uid,
                        email: req.body.email,
                        profilepic: req.body.image,
                        provider: req.body.provider
                    });
                    User.EUserFG(newUser, (err, user) => {
                        if (err) {
                            return res.json({
                                success: false
                            });
                        } else {
                            return res.json({
                                success: true
                            });
                        }
                    });                   
                }else if(user == ""){
                    //email not found
                    let newUser = new User({
                        r_id: shortid.generate(),
                        fg_id: req.body.uid,
                        name: req.body.name,
                        email: req.body.email,
                        gender: req.body.gender,
                        profilepic: req.body.image,
                        provider: req.body.provider,
                        gender: req.body.gender,
                        emailVerify: true
                        //password: shortid.generate()
                    });
                    User.NEUserFG(newUser, (err, user) => {
                        if (err) {
                            return res.json({
                                success: false
                            });
                        } else {
                             return res.json({
                                success: true
                            });
                        }
                    });
                }
            });            
        }
    });
    
}); 

userrouter.post('/gi', (req, res, next) => {    
    let newUser = new User({
        gameInterest: req.body.gamenm
    });    
    User.getUserGameInterest(newUser, (err, user) => {
        if (err) throw err;
        return res.json({
            user
        });
    });
    
});

userrouter.post('/interest', (req, res, next) => {    
    let newUser = new User({
        gameInterest: req.body.gamenm
    });    
    User.getUserGameInterest(newUser, (err, user) => {
        if (err) throw err;
        return res.json({
            user
        });
    });
    
});



//reset
userrouter.post('/reset', (req, res, next) => {
    var passwordGN = generator.generate({
        length: 10,
        numbers: true
    });
    let newUser = new User({
        email: req.body.email,
        password: passwordGN
    });
    User.getUserFoundByEmail(newUser.email, (err, user) => {
        if (err) throw err;
        if (user) {
            User.updateUser(newUser, (err, user) => {
                if (err) {
                    return res.json({
                        success: false,
                        msg: 'Failed to Reset Password'
                    });
                } else {
                    const emailinfo = {
                        email:req.body.email,
                        subject:'Game apps : Forgot Password',
                        body:`
                            <p>Hi `+ req.body.email +`,</p>                                
                                <p>We heard that you lost your Gameapps password. Sorry about that!</p>
                                <p>We have processed your request for password retrieval. Your account details are mentioned below</p>
                                <label>Your Register Email Address: </label>
                                <h3 styles="margin:0px;">`+ req.body.email +`</h3>
                                <label>Your New Password: </label>
                                <h3 styles="margin:0px;">`+ passwordGN +`</h3>

                                <p>Please keep them safe. </p>
                                <p>For any further queries/assistance, kindly mail us at denishp83test@gmail.com</p>                                
                                <p>Enjoy!</p>
                                <p>The Team Annabel.in</p>
                         `
                    };
                    usermail(emailinfo);
                    return res.json({
                        success: true,
                        msg: 'User reset password'
                    });
                }
            });
        } else {
            return res.json({
                success: false,
                msg: 'Email is Not Exist...'
            });
        }
    });
});

// Authenticate
userrouter.post('/authenticate', (req, res, next) => {
    //console.log(req);
    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err, user) => {
        if (err) throw err;
        if (!user) {
            return res.json({
                success: false,
                msg: 'User not found'
            });
        }

        User.comparePassword(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                const token = jwt.sign(user, config.secret, {
                    expiresIn: 604800 // 1 week
                });
                //console.log(token);
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    user: {
                        id: user._id,
                        name: user.name,
                        username: user.username,
                        email: user.email
                    }
                });
            } else {
                return res.json({
                    success: false,
                    msg: 'Wrong password'
                });
            }
        });
    });
});

//online count
userrouter.get('/home', (req, res, next) => {
    User.getUserCount((err, user) => {
        if (err) throw err;
        return res.json({
            user
        });
    });
});


// Profile  
userrouter.get('/profile', userpassport.authenticate('jwt', { session: false }), (req, res, next) => {    
    return res.json({
        user: req.user
    });
});

//Profile verify 
userrouter.post('/profiles', (req, res, next) => {
    let newUser = new User({
        email: req.body.email,
        emailVerify: req.body.emailVerify
    });
    User.getMatchVerify(newUser, (err, user) =>{
        if (err) throw err;
        console.log(user);
        if(user == ""){
            // not verify
            return res.json({
                success: false,
                msg: 'not verify'
            });
        }else{
            // verify
            return res.json({
                success: true,
                msg: 'verify'
            });
        }        
    });
});

userrouter.post('/verifyemail', (req, res, next) => {
    let newUser = new User({
        email: req.body.email,
        emailVerify:req.body.emailVerify
    }); 
    User.getUserFoundByEmailAid(newUser, (err, user) => {
        if(err) throw err;
        if(user){ 
            let newUser = new User({
                email:req.body.email, 
                emailVerify:req.body.emailVerify
            });            
            User.updateVerification(newUser, (err, user) => {
                if (err) {
                    return res.json({
                        success: false,
                        msg: 'Failed to Verify Email!'
                    });
                } else {
                    return res.json({
                        success: true,
                        msg: 'Verification Successfully!'
                    });
                }
            });
        }else{
            return res.json({
                success: false,
                msg: 'Failed to Verify Email!'
            });
        }

    });
});

// router.post('/frdDts', (req, res, next) => {
//     let newUser = new User({
//         _id : req.body.id
//     });
//     User.getUserfrdDt(newUser, (err, user) => {
//         if(err) throw err;
//         return res.json({
//             user: user
//         });
//     });
// });
//////////////////////////////////////////////////////////

// Save gamelist
//router.post('/games', (req, res, next) => {
    // let newGame = new Game({
    //     g_id: shortid.generate(),
    //     name: "Left4 Dead2",
    //     platform: "ps3,ps4,xbox 360,xbox one",
    //     gamepic: "http://gridom.com/sites/default/files/styles/game_logo_small/public/d3ros_0_0.jpg?itok=EKqL-cUs",
    //     Desc: "",
    //     shortDesc: "How can you make Horadric hamburgers if there is no cow level?",
    //     characters: "Coach, Rochelle, Nick"
    // });
    // Game.saveGamelist(newGame, (err, game) => {
    //   if(err){
    //       return res.json({success: false, msg:'Failed to Insert'});
    //     } else {
    //       return res.json({success: true, msg:'Insert Successfully!'});
    //     }
    // });
    // Game.updateGamelist(newGame, (err, game) => {
    //     if (err) {
    //         return res.json({
    //             success: false,
    //             msg: 'Failed to Update'
    //         });
    //     } else {
    //         return res.json({
    //             success: true,
    //             msg: 'Update Successfully!'
    //         });
    //     }
    // });
//});

// Get gamelist
userrouter.get('/games', (req, res, next) => {
    Game.getAllGamelist((err, game) => {
        if (err) throw err;
        return res.json({
            game
        });
    });
});

// Get gameDetails
userrouter.get('/games/:id', (req, res, next) => {
    const id = req.params.id;
    Game.getGameDetail(id, (err, game) => {
        if (err) throw err;
        return res.json({
            game
        });
    });
});


///////////////////////////////////////////////////////////////////

userrouter.get('/lfg', (req, res, next) => {
    postGame.getAllPostGlist((err, postgame) => {
        if (err) throw err;
        return res.json({
            postgame
        });
    });
});


userrouter.post('/activity', (req, res, next) => {
    let newPostGame = new postGame({
        name: req.body.username
    });

    postGame.getActivityPostGlist(newPostGame, (err, postgame) => {
        if (err) throw err;
        return res.json({
            postgame
        });
    });
});

userrouter.post('/lfgs', (req, res, next) => {
    let newPostGame = new postGame({
        platform: req.body.platform,
        gamename: req.body.gamename,
        characters:req.body.characters
    });

    postGame.getFilterPostGlist(newPostGame, (err, postgame) => {
        if (err) throw err;
        return res.json({
            postgame
        });
    });
});

userrouter.post('/repost', (req, res, next) => {
    let newPostGame = new postGame({
        _id: req.body._id,
        recDate:new Date()     
    });
    
    postGame.repostrquery(newPostGame, (err, postgame) => {
        if (err) {
            return res.json({ success: false });
        } else {
            return res.json({ success: true });
        }
    });
});

userrouter.post('/delpost', (req, res, next) => {
    let newPostGame = new postGame({
        _id: req.body._id,
        recDate:new Date()    
    });
    
    postGame.Delpostrquery(newPostGame, (err, postgame) => {
        if (err) {
            return res.json({ success: false });
        } else {
            return res.json({ success: true });
        }
    });
});


userrouter.post('/lfg', (req, res, next) => {
    let newPostGame = new postGame({
        gp_id: shortid.generate(),
        name: req.body.name,
        gamename: req.body.gamename,
        region: req.body.region,
        platform: req.body.platform,
        language: req.body.language,
        activity: req.body.activity,
        note: req.body.note,
        lookingfor: req.body.lookingfor,
        mic: req.body.mic,
        characters: req.body.characters,
        recDate: new Date(),
        isActive: false
    });
    postGame.savePostGame(newPostGame, (err, postgame) => {
        if (err) {
            return res.json({ success: false });
        } else {
            return res.json({ success: true });
        }
    });
});


module.exports = userrouter;